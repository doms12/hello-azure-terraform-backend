#!/usr/bin/env bash
set -eo pipefail
set -a
. ./init-vars-list.txt
set +a

printf "\n > Getting ARM access key for Terraform...\n\n"
ARM_ACCESS_KEY=$(az keyvault secret show --name "$SECRET_NAME" --vault-name "$TF_KEYVAULT_NAME" --query value -o tsv)

if export ARM_ACCESS_KEY ; then
  printf '\n > Happy terraforming!\n\n'
else
  printf '\n > Something went wrong...\n\n'
fi
