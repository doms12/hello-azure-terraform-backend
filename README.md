Base Terraform remote state storage setup for Azure
===================================================

Description
-----------

Creates dedicated Resource Group for Terraform with the following resources:

- encrypted storage account
- storage container
- keyvault
- secret with ARM access key

Terraform to use as central service to control all resources within Azure subscription.


Storage Features
----------------
- **Storage account encryption type:** Microsoft Managed Keys
- **Authentication method:** Access key
- **Access level:** Private
- **Storage type:** Blob


Usage
-----

**Meet prerequisites**

Azure CLI installed and configured to use target Azure subscription.
```bash
az account show
```

**Set variables**

Files:

- *init-vars-list.txt*
- *terraform.tf*
- *main.tf*
- *variables.tf*


Tricky variables to pay attention to upon creation (unique name):
```
TF_STATE_STORAGE_ACCOUNT_NAME="tfstatestorage21669"
TF_KEYVAULT_NAME="Terraform25960"
```

Run the following bash script:

```bash
./init-remote-state-backend.sh
```

Additional handy scripts:

- `./get-arm-access-key.sh` - to export arm access key into terminal session.
- `./init-terraform.sh` - to init or re-init terraform.
